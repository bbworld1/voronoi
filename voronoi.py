import numpy as np
from scipy.spatial import Voronoi, voronoi_plot_2d
import shapely
import shapely.geometry
from shapely.ops import nearest_points
import matplotlib
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111)

points = np.array([[-1, -1], [-1, 11], [11, -1], [11, 11]])
robots = np.random.rand(50, 2) * 10
robot_goals = np.random.rand(50, 2) * 10

def distance_to(point1, point2):
    asq = (point1[0] - point2[0])**2
    bsq = (point1[1] - point2[1])**2

    return np.sqrt(asq + bsq)

def rotate(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.

    The angle should be given in radians.
    """
    ox, oy = origin
    px, py = point

    qx = ox + np.cos(angle) * (px - ox) - np.sin(angle) * (py - oy)
    qy = oy + np.sin(angle) * (px - ox) + np.cos(angle) * (py - oy)
    return qx, qy


for i in range(1000):
    ax.cla()
    vor = Voronoi([*points, *robots])
    patches = []
    for c, robot in enumerate(robots):
        vor_robot = shapely.geometry.Polygon([ vor.vertices[x] for x in vor.regions[vor.point_region[4 + c]] ]).buffer(-0.1)
        if isinstance(vor_robot, shapely.geometry.MultiPolygon):
            vor_robot = vor_robot[0]

        try:
            nearest_robot = nearest_points(vor_robot, shapely.geometry.Point(robot_goals[c]))[0].coords[0]
        except ValueError:
            nearest_robot = robot_goals[c]

        try:
            patches.append(matplotlib.patches.Polygon(list(vor_robot.exterior.coords)))
        except ValueError:
            pass
        robot_nearest_line = matplotlib.lines.Line2D([robot[0], nearest_robot[0]], [robot[1], nearest_robot[1]], linestyle="--")
        robot_goal_line = matplotlib.lines.Line2D([robot[0], robot_goals[c][0]], [robot[1], robot_goals[c][1]], linestyle="-")
        ax.add_line(robot_nearest_line)
        ax.add_line(robot_goal_line)

        robot_dx = float((nearest_robot[0] - robot[0]) / 25)
        robot_dy = float((nearest_robot[1] - robot[1]) / 25)

        if distance_to(robot, nearest_robot) < 0.1 and distance_to(robot, robot_goals[c]) > 0.2:
            print("Negotiating deadlock")
            robot_goal = rotate(robot, robot_goals[c], -np.pi/3)
            nearest_robot = nearest_points(vor_robot, shapely.geometry.Point(robot_goal))[0].coords[0]
            robot_nearest_line2 = matplotlib.lines.Line2D([robot[0], nearest_robot[0]], [robot[1], nearest_robot[1]], linestyle="--", color="red")
            robot_goal_line2 = matplotlib.lines.Line2D([robot[0], robot_goal[0]], [robot[1], robot_goal[1]], linestyle="-", color="red")
            ax.add_line(robot_nearest_line2)
            ax.add_line(robot_goal_line2)
            robot_dx = float((nearest_robot[0] - robot[0]) / 25)
            robot_dy = float((nearest_robot[1] - robot[1]) / 25)

        robots[c] += [robot_dx, robot_dy]

    p = matplotlib.collections.PatchCollection(patches)
    p.set_color([1, 0.8, 0.8])
    ax.add_collection(p)
    voronoi_plot_2d(vor, ax=ax)

    plt.pause(0.01)
